#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile --output-file=requirements/py3.11/tests.txt requirements/tests.in
#
acct==8.6.1
    # via
    #   idem
    #   pop-evbus
aiofiles==22.1.0
    # via
    #   acct
    #   dict-toolbox
    #   grains-universal
    #   idem
    #   pop-tree
asynctest==0.13.0
    # via -r requirements/tests.in
attrs==22.1.0
    # via pytest
cffi==1.15.1
    # via cryptography
colorama==0.4.5
    # via
    #   idem
    #   rend
cryptography==38.0.1
    # via acct
dict-toolbox==4.1.1
    # via
    #   acct
    #   grains-universal
    #   idem
    #   pop
    #   pop-config
    #   pytest-pop
    #   rend
dnspython==2.2.1
    # via -r requirements/extra/grains.txt
grains-universal==3
    # via -r requirements/extra/grains.txt
grainsv2==11.10
    # via
    #   -r requirements/extra/grains.txt
    #   grains-universal
file:.
    # via -r requirements/tests.in
idem==23.1.1
    # via
    #   grainsv2
    #   idem-posix
    #   pytest-idem
iniconfig==1.1.1
    # via pytest
jinja2==3.1.2
    # via
    #   idem
    #   rend
jmespath==1.0.1
    # via idem
lazy-object-proxy==1.7.1
    # via pop
markupsafe==2.1.1
    # via jinja2
mock==4.0.3
    # via
    #   -r requirements/tests.in
    #   pytest-pop
msgpack==1.0.4
    # via
    #   dict-toolbox
    #   pop-evbus
    #   pop-serial
nest-asyncio==1.5.6
    # via
    #   pop-loop
    #   pytest-pop
packaging==21.3
    # via pytest
pluggy==1.0.0
    # via pytest
pop-config==12.0.2
    # via
    #   acct
    #   grainsv2
    #   idem
    #   pop
    #   pytest-pop
pop-evbus==6.2.3
    # via idem
pop-loop==1.0.5
    # via
    #   idem
    #   pop
pop-serial==1.1.0
    # via
    #   acct
    #   idem
    #   pop-evbus
pop-tree==9.3.0
    # via idem
pop==24.0.1
    # via
    #   acct
    #   grainsv2
    #   idem
    #   idem-posix
    #   pop-config
    #   pop-evbus
    #   pop-loop
    #   pop-serial
    #   pop-tree
    #   pytest-pop
    #   rend
py==1.11.0
    # via pytest
pycparser==2.21
    # via cffi
pyparsing==3.0.9
    # via packaging
pytest-async==0.1.1
    # via pytest-pop
pytest-asyncio==0.18.3
    # via
    #   -r requirements/tests.in
    #   pytest-pop
pytest-idem==3.1.0
    # via -r requirements/tests.in
pytest-pop==12.0.0
    # via
    #   -r requirements/tests.in
    #   pytest-idem
pytest-subtests==0.8.0
    # via -r requirements/tests.in
pytest==7.1.3
    # via
    #   -r requirements/tests.in
    #   pytest-asyncio
    #   pytest-pop
    #   pytest-subtests
python-dateutil==2.8.2
    # via grains-universal
pyyaml==6.0.1
    # via
    #   acct
    #   dict-toolbox
    #   idem
    #   pop
    #   rend
rend==6.5.0
    # via
    #   acct
    #   grainsv2
    #   idem
    #   pop-config
    #   pop-evbus
    #   pop-tree
six==1.16.0
    # via python-dateutil
sniffio==1.3.0
    # via pop-loop
toml==0.10.2
    # via
    #   idem
    #   rend
tomli==2.0.1
    # via pytest
tqdm==4.64.1
    # via idem
uvloop==0.17.0
    # via idem
wheel==0.37.1
    # via idem
