import os.path

import pytest


@pytest.mark.asyncio
async def test_cmd_json_output(mock_hub, hub):
    os.chdir(os.getcwd())
    command_file_path = os.path.abspath("./tests/unit/exec/decrypt-secret.sh")
    ret = await hub.exec.cmd.run(command_file_path)
    print("Ret", ret, command_file_path)
    assert ret["result"]
    assert ret["ret"]["stdout"]
    assert isinstance(ret["ret"]["stdout"], dict)
    assert ret["ret"]["stdout"]["secret"] == "ABCDEfgHIJ3ree"
