=================
Executing Command
=================

Idem-posix plugin allows to execute commands on a posix systems and return the output as a string.

Example of using cmd.run state in SLS:

.. code-block:: sls

    # Execute "ls -l" command
    my_state_name:
      cmd.run:
        cmd: ls -l
        cwd: /
        shell: False
        env:
          ENV_VAR_1: ENV_VAL_1
          ENV_VAR_2: ENV_VAL_2
        timeout: 100
        render_pipe:
        kwargs:

Example of using cmd.run with Idem CLI:

.. code-block:: bash

    idem exec cmd.run "shell command --with-flags" cwd=/home render_pipe=json timeout=10
