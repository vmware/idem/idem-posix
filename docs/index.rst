.. idem-posix documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to idem-posix's Documentation!
======================================


.. toctree::
   :maxdepth: 2
   :glob:

   topics/idem-posix
   tutorial/index

.. include:: /_includes/reference-toc.rst

.. toctree::
   :caption: Get Involved
   :maxdepth: 2
   :glob:

   topics/contributing
   topics/license
   Project Repository <https://gitlab.com/vmware/idem/idem-posix>

Indices and tables
==================

* :ref:`modindex`
