import copy

from tests.integration.states import run_sls

STATE_PWD = """
pwd:
  cmd.run:
    - cmd: pwd
"""


def test_simple_run(hub):
    managed_state = {}
    running = run_sls(STATE_PWD, managed_state=managed_state)
    ret = running["cmd_|-pwd_|-pwd_|-run"]
    assert ret["result"], ret["comment"]
    assert ret["old_state"] is None, ret["comment"]
    assert ret["new_state"]["retcode"] == 0, ret["comment"]
    assert ret["new_state"]["state"] == {}, ret["comment"]
    assert ret["new_state"]["stderr"] == "", ret["comment"]
    assert ret["new_state"]["stdout"], ret["comment"]
    enforced_state = copy.copy(managed_state["cmd_|-pwd_|-pwd_|-"])
    assert ret["new_state"] == enforced_state, ret["comment"]


def test_enforced_state(hub):
    managed_state = {}
    ret1 = run_sls(STATE_PWD, managed_state=managed_state)
    assert ret1, ret1["comment"]
    old_state1 = copy.copy(managed_state["cmd_|-pwd_|-pwd_|-"])
    ret2 = run_sls(STATE_PWD, managed_state=managed_state)
    assert ret2, ret2["comment"]
    old_state2 = copy.copy(managed_state["cmd_|-pwd_|-pwd_|-"])
    assert old_state1 == old_state2, ret2["comment"]


STATE_ECHO_JSON_LIST = """
echo:
  cmd.run:
    - cmd: echo '["1", "2", "3"]'
    - render_pipe: json
    - shell: true
"""


def test_render_pipe_list(hub):
    managed_state = {}
    running = run_sls(STATE_ECHO_JSON_LIST, managed_state=managed_state)
    ret = list(running.values())[0]
    assert ret["result"], ret["comment"]
    assert ret["old_state"] is None, ret["comment"]
    assert ret["new_state"]["state"] == ["1", "2", "3"], ret["comment"]
    key = next(iter(managed_state))
    enforced_state = copy.copy(managed_state[key])
    assert ret["new_state"] == enforced_state, ret["comment"]


STATE_ECHO_JSON_DICT = """
echo:
  cmd.run:
    - cmd: >
        echo '{"1": "2"}'
    - render_pipe: json
    - shell: true
"""


def test_render_pipe_dict(hub):
    managed_state = {}
    running = run_sls(STATE_ECHO_JSON_DICT, managed_state=managed_state)
    ret = list(running.values())[0]
    assert ret["result"], ret["comment"]
    assert ret["old_state"] is None, ret["comment"]
    assert ret["new_state"]["state"] == {"1": "2"}, ret["comment"]
    key = next(iter(managed_state))
    enforced_state = copy.copy(managed_state[key])
    assert ret["new_state"] == enforced_state, ret["comment"]


STATE_CMD_LIST = """
echo:
  cmd.run:
    - cmd:
        - ls
        - -l
        - -a
"""


def test_cmd_list(hub):
    managed_state = {}
    running = run_sls(STATE_ECHO_JSON_DICT, managed_state=managed_state)
    ret = list(running.values())[0]
    assert ret["result"], ret["comment"]
    assert ret["old_state"] is None, ret["comment"]
    assert ret["new_state"]["stderr"] == "", ret["comment"]
    assert ret["new_state"]["stdout"], ret["comment"]
    assert ret["new_state"]["retcode"] == 0, ret["comment"]
    key = next(iter(managed_state))
    enforced_state = copy.copy(managed_state[key])
    assert ret["new_state"] == enforced_state, ret["comment"]


STATE_FAILED_CMD = """
echo:
  cmd.run:
    - cmd: this_is_not_a_real_command
"""


def test_cmd_fail(idem_cli, tests_dir):
    running = idem_cli("state", tests_dir / "sls" / "fail.sls")
    ret = list(running.json.values())[0]
    assert not ret["result"], ret["comment"]
    assert ret["old_state"] is None, ret["comment"]
    assert ret["new_state"]["retcode"] == 2, ret["comment"]
    assert "FileNotFoundError" in ret["new_state"]["stderr"], ret["comment"]


STATE_NONZERO_CMD = """
false:
  cmd.run:
    - cmd: 'false'
"""


def test_cmd_non_zero_retcode_fails(idem_cli, tests_dir):
    managed_state = {}
    running = run_sls(STATE_NONZERO_CMD, managed_state=managed_state)
    ret = list(running.values())[0]
    assert not ret["result"], ret["comment"]
    assert ret["new_state"]["retcode"] == 1, ret["comment"]


STATE_NONZERO_CMD_EXPECTED = """
false:
  cmd.run:
    - cmd: 'false'
    - success_retcodes:
      - 1
"""


def test_cmd_non_zero_retcode_passes(idem_cli, tests_dir):
    managed_state = {}
    running = run_sls(STATE_NONZERO_CMD_EXPECTED, managed_state=managed_state)
    ret = list(running.values())[0]
    assert ret["result"], ret["comment"]
    assert ret["new_state"]["retcode"] == 1, ret["comment"]


def test_no_ctx_test(idem_cli, tests_dir):
    # cmd.run in "test" mode is skipped
    without_test = idem_cli("state", tests_dir / "sls" / "no_ctx_test.sls")
    assert without_test.json["cmd_|-no_ctx_|-no_ctx_|-run"]["result"] is True

    with_test = idem_cli("state", tests_dir / "sls" / "no_ctx_test.sls", "--test")
    assert with_test.json["cmd_|-no_ctx_|-no_ctx_|-run"]["result"] is True

    assert without_test.json["cmd_|-no_ctx_|-no_ctx_|-run"]["new_state"]
    assert {} == with_test.json["cmd_|-no_ctx_|-no_ctx_|-run"]["new_state"]


def test_ctx_test(idem_cli, tests_dir):
    # cmd.run in "test" mode is skipped
    # 'ignore_test' is a flag that overrides the 'test' mode
    with_test = idem_cli("state", tests_dir / "sls" / "ignore_test.sls", "--test")
    output = with_test.json
    assert len(output) == 3

    # ctx.test - not running
    test1_tag = "cmd_|-ctx_with_test_|-ctx_with_test_|-run"
    assert output[test1_tag]["result"] is True
    assert "test1" not in output[test1_tag]["comment"][0]
    assert "does not run" in output[test1_tag]["comment"], output["comment"]

    # ctx.test and ignore_test flag is true - running
    test2_tag = "cmd_|-ctx_with_test_ignore_true_|-ctx_with_test_ignore_true_|-run"
    assert output[test2_tag]["result"] is True
    assert "test2" in output[test2_tag]["comment"][0]

    # ctx.test and ignore_test flag is false - not running
    test3_tag = "cmd_|-ctx_with_test_ignore_false_|-ctx_with_test_ignore_false_|-run"
    assert output[test3_tag]["result"] is True
    assert "test3" not in output[test3_tag]["comment"][0]
    assert "does not run" in output[test3_tag]["comment"], output["comment"]
